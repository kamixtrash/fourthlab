import java.util.Objects;

public class CountMapEntry<T> {

    private Integer dataCount;
    private final T data;

    public CountMapEntry(T data) {
        this.dataCount = 1;
        this.data = data;
    }

    public Integer getDataCount() {
        return dataCount;
    }

    public void setDataCount(Integer dataCount) {
        this.dataCount = dataCount;
    }

    public T getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountMapEntry<?> countMapEntry1 = (CountMapEntry<?>) o;
        return Objects.equals(data, countMapEntry1.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataCount, data);
    }
}
