import java.util.ArrayList;
import java.util.Map;

public interface CountMap<T> {
    void add(T o);
    int getCount(T o);
    int remove(T o);
    int size();
    void addAll(CountMap<T> src);
    Map<T, Integer> toMap();
    void toMap(Map<T, Integer> dest);
    ArrayList<T> getValues();
}
