import java.util.*;
import java.util.stream.Collectors;

public class CountMapImpl<T> implements CountMap<T> {

    private final Set<CountMapEntry<T>> countMap = new HashSet<>();

    @Override
    public void add(T o) {
        if (countMap.isEmpty()) {
            countMap.add(new CountMapEntry<>(o));
            return;
        }

        for (CountMapEntry<T> value : countMap) {
            if (value.getData().equals(o)) {
                value.setDataCount(value.getDataCount() + 1);
                return;
            }
        }

        countMap.add(new CountMapEntry<>(o));
    }

    @Override
    public int getCount(T o) {
        for (CountMapEntry<T> value : countMap) {
            if (value.getData().equals(o)) {
                return value.getDataCount();
            }
        }

        return 0;
    }

    @Override
    public int remove(T o) {
        int count = getCount(o);
        countMap.removeIf(value -> value.getData().equals(o));
        return count;
    }

    @Override
    public int size() {
        return countMap.size();
    }

    @Override
    public void addAll(CountMap<T> src) {
        for (T value : src.getValues()) {
            this.add(value);
        }
    }

    @Override
    public Map<T, Integer> toMap() {
        Map<T, Integer> map = new HashMap<>();

        for (CountMapEntry<T> value : countMap) {
            map.put(value.getData(), value.getDataCount());
        }

        return map;
    }

    @Override
    public void toMap(Map<T, Integer> dest) {
        for (CountMapEntry<T> value : countMap) {
            dest.put(value.getData(), value.getDataCount());
        }
    }

    public ArrayList<Integer> getKeys() {
        return (ArrayList<Integer>) countMap.stream().map(CountMapEntry::getDataCount).collect(Collectors.toList());
    }

    @Override
    public ArrayList<T> getValues() {
        return (ArrayList<T>) countMap.stream().map(CountMapEntry::getData).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountMapImpl<?> countMap1 = (CountMapImpl<?>) o;
        return Objects.equals(countMap, countMap1.countMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countMap);
    }
}
