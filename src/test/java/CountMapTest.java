import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CountMapTest {

    static CountMap<String> stringCountMap;
    static CountMap<Integer> integerCountMap;
    static HashMap<Integer, Integer> convertedHashMap;
    static HashMap<Integer, Integer> rightHashMap;

    @BeforeEach
    public void prepareData() {
        stringCountMap = new CountMapImpl<>();
        integerCountMap = new CountMapImpl<>();
        convertedHashMap = new HashMap<>();
        rightHashMap = new HashMap<>();
        rightHashMap.put(3, 4);
        rightHashMap.put(4, 2);
        rightHashMap.put(5, 1);

        stringCountMap.add("foo");
        stringCountMap.add("foo");
        stringCountMap.add("bar");
        stringCountMap.add("bar");
        stringCountMap.add("foo");
        stringCountMap.add("bar");
        stringCountMap.add("foo");

        integerCountMap.add(3);
        integerCountMap.add(4);
        integerCountMap.add(5);
        integerCountMap.add(4);
        integerCountMap.add(3);
        integerCountMap.add(3);
        integerCountMap.add(3);
    }

    @Test
    public void addTest_String() {
        assertEquals(4, stringCountMap.getCount("foo"));
        assertEquals(3, stringCountMap.getCount("bar"));
        assertEquals(2, stringCountMap.size());
    }

    @Test
    public void addTest_Integer() {
        assertEquals(4, integerCountMap.getCount(3));
        assertEquals(3, integerCountMap.size());
    }

    @Test
    public void removeTest_String() {
        assertEquals(4, stringCountMap.remove("foo"));
    }

    @Test
    public void toMapTest_String() {
        assertEquals("{bar=3, foo=4}", stringCountMap.toMap().toString());
    }
}
